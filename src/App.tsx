import React from 'react';
import { BrowserRouter, Route, Routes} from 'react-router-dom'
import Home from './Components/Home/Home';
import Film from './Components/Film/Film';
import Showtime from './Components/Showtime/Showtime';
import './App.scss'
import './Components/common.css'
import MovieDetail from './Components/MovieDetail/MovieDetail';


function App() {
  return (
    <div className='cinema'>
      
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/Film' element={<Film />} />
          <Route path='/Showtime' element={<Showtime />} />
          <Route path='/Films/:id' element={<MovieDetail/>}/>

        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;
