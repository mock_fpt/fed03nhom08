import ActionFilm from "../Action/ActionFilm";

const initialState = {
    FilmInfo: null
}

const rdcCity = (state = initialState, { type, payload }) => {
    switch (type) {

        case ActionFilm.SET_DATA:
            return {
                ...state,
                cityInfo: payload,
                status: ""
            }
        default:
            return state;
    }
}

export default rdcCity;