import {call, put, takeEvery} from 'redux-saga/effects'

async function ShowtimeApi(){
    let res = await fetch(`https://teachingserver.onrender.com/cinema/nowAndSoon`)
    let data = await res.json();
    return data;
}

// // function* GetShowtimeData({type: 'SET_DATA', payload}){
// //     var lsShowtimeData = yield call(ShowtimeApi, payload);
// // }


// // function* mySaga() {
// //     yield takeEvery("SET_DATA", GetShowtimeData)
// // }

// export default mySaga;