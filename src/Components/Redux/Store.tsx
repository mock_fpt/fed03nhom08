import rdcShowtime from './Reducer/rdcShowtime'
import saga from 'redux-saga'
// import middleSaga from './Saga/MiddleSaga'

const redux = require("redux")
const middleware = saga();
const GlocalState = {
  ShowTimeState: rdcShowtime
}

const allReducer = redux.combineReducers(GlocalState);
export default redux.createStore(allReducer, redux.applyMiddleware(middleware));
// middleware.run(middleSaga)
