// export interface Ticket {
    
// }

export interface Session {
    name: string;
    url: string;
    id: string;
    cinemaId: string;
    scheduledFilmId: string;
    sessionId: string;
    areaCategoryCodes: any[];
    isAllocatedSeating: boolean;
    allowChildAdmits: boolean;
    seatsAvailable: number;
    allowComplimentaryTickets: boolean;
    eventId: string;
    priceGroupCode: string;
    screenName: string;
    screenNameAlt: string;
    screenNumber: number;
    cinemaOperatorCode: string;
    formatCode: string;
    formatHopk: string;
    salesChannels: string;
    sessionAttributesNames: any[];
    conceptAttributesNames: any[];
    allowTicketSales: boolean;
    hasDynamicallyPricedTicketsAvailable: boolean;
    playThroughId?: any;
    sessionBusinessDate: Date;
    sessionDisplayPriority: number;
    groupSessionsByAttribute: boolean;
    soldoutStatus: number;
    typeCode: string;
    dayOfWeekLabel: string;
    dayOfWeekKey: string;
    showDate: string;
    showTime: string;
    caption: string;
    code: string;
    version: string;
    sessions: Session[];
}

// export interface Bundle {
    
// }

// export interface Date {
    
// }

export interface RootObject {
    id: string;
    slug: string;
    name: string;
    vistaName: string;
    description: string;
    code: string;
    order: number;
    phone: string;
    address: string;
    cityId: string;
    mapEmbeb: string;
    status: number;
    reward: number;
    createdAt: Date;
    updatedAt: Date;
    createdBy: string;
    updatedBy: string;
    oldId?: number;
    imageUrls: string[];
    imageLandscape: string;
    imagePortrait: string;
    longitude: string;
    latitude: string;
    cityIds?: any;
    dates: Date[];
}