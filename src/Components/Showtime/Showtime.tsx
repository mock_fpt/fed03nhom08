import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import '../common.css'
import '../Showtime/Showtime.scss'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import { IMovie } from '../Home/Overview/IMovies'


export default function Showtime() {
    
    const nav = useNavigate()
    const [lsFilmShowing, setLsFilmShowing] = useState<Array<IMovie>>([])
     const [lsFilmSoon, setLsFilmSoon] = useState<Array<IMovie>>([])

    useEffect(()=>{
        fetch('https://teachingserver.onrender.com/cinema/nowAndSoon')
        .then(res => res.json())
        .then(data =>{
            setLsFilmShowing(data.movieShowing);
            setLsFilmSoon(data.movieCommingSoon);
        
        fetch('')
        })
    }, [])

    const ShowFilm = (id: any) => {
        nav('/Films/' + id)
    }

    const optionRap = () =>{
        
    }

  return (
    <div>
    <Header />
    <div className='showTimes mainSize'>
        <div className='optionMovie'>
            <h4 className='tx'>Chọn Phim</h4>
            <ul className='itemMovie'>
            {
                lsFilmShowing.map((n, i)=>{
                    return <li key={i} className='tx' onClick={optionRap}>
                    <div className='leftItem'>
                        <img className='img-cover' width={60} height={40} src={n.imageLandscape} alt="" />
                        <div className='detailName'>
                        <p>{n.name}</p>
                        <p className='subN'>{n.subName}</p>
                        </div>
                    </div>
                    <p>+</p>
                    </li>
                })
            }
            {
                lsFilmSoon.map((n, i)=>{
                    return <li key={i} className='tx'>
                    <div className='leftItem'>
                        <img className='img-cover' width={60} height={40} src={n.imageLandscape} alt="" />
                        <div className='detailName'>
                        <p>{n.name}</p>
                        <p className='subN'>{n.subName}</p>
                        </div>
                    </div>
                    <p>+</p>
                    </li>
                })
            }
            </ul>
        </div>
        
        <div className='optionMovie'>
            <h4 className='tx'>Chọn Rạp</h4>
            <ul className='itemCnm'>
                <li>
                    Galaxy Nguyễn Du
                </li>
                <li>
                    Galaxy Nguyễn Du
                </li>
                <li>
                    Galaxy Nguyễn Du
                </li>
            </ul>
        </div>

        <div className='optionSet'>
            <h4 className='tx'>Chọn Xuất</h4>
            <ul>
                <li>
                <p>Thứ 4, sad</p>
                <div className='detail'>
                    <label>2D - Phụ đề</label>
                    <div className='timeDetail'>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                        <a href="">10:15</a>
                    </div>
                </div>
                </li>
            </ul>
        </div>
    </div>
    <Footer />

    </div>
  )
}
