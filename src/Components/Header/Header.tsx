import React from 'react';
import './Header.scss'
// import './Header.css';


export default function Header() {
    return (
        <div className='Header tx'>
            <div className='mainSize navBar'>
                <div className='containerHearder'>
                    <div className='logo'>
                        <img width={100} height={100} src={require('../../Image/logo.png')} alt="" />
                    </div>
                    <div className='menuInfo'>
                        <div className='buttonTop'>
                            <div className='row'>
                                <div className='buttonRow'>
                                    <a className='buyTicket' href="#">
                                        <img src={require('../../Image/dat-ve-ngay.png')} alt="" />
                                    </a>
                                    <div className='tagMiddle'>
                                        <form action="">
                                            <div className='input-menu'>
                                                <input type="text" placeholder='Tìm kiếm' />
                                                {/* <button><i className='fa-solid fa-magnifying-glass'></i></button> */}
                                            </div>
                                        </form>
                                        <a className='language' href="#"><img width={36} height={36} src={require('../../Image/vn.png')} alt="" /></a>
                                    </div>
                                    <div className='account'>
                                        <a className='icFace' href="#">
                                            <img src={require('../../Image/icon-fb.png')} alt="" />
                                        </a>
                                        <a className='icYtb' href="#">
                                            <img src={require('../../Image/icon-ytb.png')} alt="" />
                                        </a>
                                        <a className='login' href="#Login"> <img src={require('../../Image/dang-nhap.png')} alt="Đăng nhập" />
                                            <span>Đăng nhập</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className='navbav'>
                                <ul className='nav'>
                                    <li><a className='start-home' href="/">Trang chủ</a></li>
                                    <li><a className='start-movie' href="/Film">Phim</a></li>
                                    <li><a className='start-showtime'href="/Showtime">Lịch chiếu</a></li>
                                    <li><a href="#">Giá vé</a></li>
                                    <li><a href="#">Thành viên</a></li>
                                    <li><a href="#">Ưu đãi - Sự kiện</a></li>
                                    <li><a href="#">Đánh giá phim</a></li>
                                    <li><a href="#">Giới thiệu</a></li>
                                    <li><a href="#">Dịch vụ</a></li>
                                </ul>
                                <div className='animation start-home'></div>
                                <ul>
                                    <li>
                                        <div>
                                            <div>
                                                bell
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

        </div>

    )
}
