import React, { useEffect, useState } from 'react'
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
// import { useNavigate } from 'react-router-dom'
import { IMovie } from '../Home/Overview/IMovies';
import '../Film/Film.scss'

export default function Overview() {
  const [lsShow, setLsShow] = useState(true);
  const [lsFilmShowing, setLsFilmShowing] = useState<Array<IMovie>>([])
  const [lsFilmSoon, setLsFilmSoon] = useState<Array<IMovie>>([])
  // const nav = useNavigate()

  
  useEffect(() => {
    fetch("https://teachingserver.onrender.com/cinema/nowAndSoon")
      .then(res => res.json())
      .then(data => {
        setLsFilmShowing(data.movieShowing);
        setLsFilmSoon(data.movieCommingSoon);

      })
  }, [])

  

  //   const ShowFilm = (id: any) => {
  //     nav('/Films/' + id)
  // }
  return (
    <div>
      <Header />
      <div className='Films'>
      <div className='contain'>
        <div className='btnMovie'>
          <div className='btnShowing'>
            <button onClick={() => setLsShow(true)}>
              <img width={62} height={55} src={require('../../Image/Movie/icon-ticket.png')} alt="Nguyễn Đức Cường" />
              <span>Phim đang chiếu</span>
            </button>
          </div>

          <div className='btnSoon'>
            <button onClick={() => setLsShow(false)}>
              <img width={45} height={55} src={require('../../Image/Movie//icon-sap-chieu.png')} alt="Nguyễn Đức Cường" />
              <span>Phim sắp chiếu</span>
            </button>
          </div>
        </div>

        <div className='movieList'>
          {
            lsShow && lsFilmShowing?.map((v, i) => {
              return <div key={i} className='card'>
                <img  src={v.imagePortrait} alt="" />
                <div className='detailCard'>
                  <h2><a href="#a">{v.subName}</a></h2>
                  <p>Khởi chiếu{v.startdate}</p>
                </div>
              </div>
            })
          }
          {
            !lsShow && lsFilmSoon?.map((v, i) => {
              return <div key={i} className='card'>
                <img  src={v.imagePortrait} alt="" />
                <div className='detailCard'>
                  <h2><a href="#a">{v.subName}</a></h2>
                  <p>Khởi chiếu{v.startdate}</p>
                </div>
              </div>
            })
          }
        </div>
      </div>
    </div>
    <Footer />
    </div>
  )

}

