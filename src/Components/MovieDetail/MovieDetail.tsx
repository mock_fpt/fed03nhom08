import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { IMovie } from '../Home/Overview/IMovies'
import { Ticket } from '../MovieDetail/IMovieID'
import '../MovieDetail/MovieDetail.scss'

export default function MovieDetail() {
    const { id } = useParams()
    const [Film, SetFilm] = useState<Array<IMovie>>([])
    const [Cinema, SetCinema] = useState<Array<Ticket>>([])

    useEffect(() => {
        fetch("https://teachingserver.onrender.com/cinema/nowAndSoon")
            .then(res => res.json())
            .then(data => {
                SetFilm(
                    [
                        ...data.movieShowing.filter((n:any) => n.id === id),
                        ...data.movieCommingSoon.filter((n:any) => n.id === id)
                    ][0]
                );
            }),

        fetch("https://teachingserver.onrender.com/cinema/movie/" + id)
            .then(res => res.json())
            .then(data => SetCinema(data))
    }, [id])

    return (
       <div className='detailfilm'>
        <div className='image'>
{
                    Film.map((n, i) => {
                        return <div key={i} className='Film'>
                            <img src={n.imagePortrait} />
                            <h3>{n.name}</h3>
                            <h3>{n.subName}</h3>
                            <p><b>Thời Lượng: {n.duration} Phút</b></p>
                            <p>-----</p>
                        </div>
                    })
                }
</div>
            <div className='content'>
                {
                    Cinema.map((n, i) => {
                        return <div key={i} className='cinema'>
                            <h3>Cinema: {n.vistaName}</h3>
                            <hr />
                            {
                                n.dates?.map((d:any, i2:any) => {
                                    return <div key={i2} className="CardDay">
                                        <h4>- {d.showDate}</h4>
                                        <div className='boxDate'>
                                            {
                                                d.bundles.map((b:any) => {
                                                    return <div>
                                                        <h5>Ticket {b.version}</h5>
                                                        <div>
                                                            {
                                                                b.sessions.map((t:any) => {
                                                                    return <span>
                                                                        {t.showTime}
                                                                        
                                                                    </span>
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    </div>
                                })
                            }
                        </div>
                    })
                }
            </div>
        </div>
    )
}
